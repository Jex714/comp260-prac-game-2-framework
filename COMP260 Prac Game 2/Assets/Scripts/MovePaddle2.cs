﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class MovePaddle2 : MonoBehaviour

{
    private Rigidbody rigidbody;
    public string player1_horizontal;
    public string player1_vertical;
    public float force = 50f;
    public float gravity = 1f;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }

    void FixedUpdate()
    {
        Vector3 movedir = new Vector3
            (Input.GetAxis(player1_horizontal)
            , 0,
            Input.GetAxis(player1_vertical));
        rigidbody.AddForce(movedir * force);
        {
            if (force > 15)
            { force = 15; }
        }
    }
}