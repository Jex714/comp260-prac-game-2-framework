﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class PaddleAI : MonoBehaviour
{
    private Rigidbody rigidbody;
    public GameObject Paddle;
    public float force = 50f;
    public float gravity = 1f;


    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movedir = Paddle.transform.position; //new Vector3 (Paddle.transform.position.y - transform.position.y,0,Paddle.transform.position.x - transform.position.x);
        rigidbody.AddForce(Paddle.transform.position - transform.position); //(movedir * force);

        if (force > 100)
        {
            //rigidbody.AddForce(-(movedir * force));
        }
    }
}