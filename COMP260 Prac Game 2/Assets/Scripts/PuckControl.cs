﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour
    

{
    private AudioSource audio;
    public AudioClip wallCollideClip;
    public AudioClip paddleCollideClip;
    public LayerMask paddleLayer;
    public Transform startingPos;
    private Rigidbody rigidbody;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody>();
        ResetPosition();
    }
    void OnCollisionEnter(Collision collision)
    {
        // check what we have hit
        if (paddleLayer.Contains(collision.gameObject))
        {
            // hit the paddle
            audio.PlayOneShot(paddleCollideClip);
        }
        else
        {
            // hit something else
            audio.PlayOneShot(wallCollideClip);
        }
    }
    public void ResetPosition()
    {
        // teleport to the starting position
        rigidbody.MovePosition(startingPos.position);
        // stop it from moving
        rigidbody.velocity = Vector3.zero;
    }

    //void OnCollisionEnter(Collision collision)
    //{
    //    Debug.Log("Collision Enter: " + collision.gameObject.name);
    //}
    //void OnCollisionStay(Collision collision)
    //{
    //    Debug.Log("Collision Stay: " + collision.gameObject.name);
    //}
    //void OnCollisionExit(Collision collision)
    //{
    //    Debug.Log("Collision Exit: " + collision.gameObject.name);
    //}
    // there are collision stay events when the puck is grinding on the walls, and occaisionally the player's paddle


    // Update is called once per frame
    void Update () {
		
	}
}
