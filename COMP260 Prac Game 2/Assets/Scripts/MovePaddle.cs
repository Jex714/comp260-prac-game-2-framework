﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class MovePaddle : MonoBehaviour {
private Rigidbody rigidbody;
public float speed = 20f;

    //public float force = 10f;


    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;

    }

    private Vector3 GetMousePosition()
    {
        // create a ray from the camera
        // passing through the mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // find out where the ray intersects the XZ plane
        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Vector3 pos = GetMousePosition();
        // draw the mouse ray
        Gizmos.DrawLine(Camera.main.transform.position, pos);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log( " Time = " +Time.time);
    }
    void FixedUpdate()
    {
        //Vector3 pos = GetMousePosition();
        //Vector3 dir = pos - rigidbody.position;
        //rigidbody.AddForce(dir.normalized * force);
        // the forece approach produces a much slower movemnt
        // of the three approaches, the second is more suitable
        // the force behaviour acts slowly as it is not immediately
        // moving the pick towards the cursor, rather 
        // the puck is slowly gravitating towards it

        Debug.Log(" Fixed Time = " + Time.fixedTime);
        Vector3 pos = GetMousePosition();
        Vector3 dir = pos - rigidbody.position;
        Vector3 vel = dir.normalized * speed;
        // check is this speed is going to overshoot the target
        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;
        if (move > distToTarget)
        {
            // scale the velocity down appropriately
            vel = vel * distToTarget / move;
        }
        rigidbody.velocity = vel;

    }
}


// 1) using transform to move objects will not properly use the unity 
// physics system because it won't detect collisions whereas using 
// rigidbody will detect collisions and allow for smoother movement 
// 2) transform.position as a component doesn't rely on physics and usually
// ignores them. Rigidbody.velocity isn't something that you can modify as easily 
// as rigidbody.addforce. Rigidbody.position is used before rigidbody.transform
// as the physics steps will be calculated more frequently and transform will update
// accordingly.velocity is speed with a direction